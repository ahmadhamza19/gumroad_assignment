(function () {
  const buttons = document.querySelectorAll(".gumroad-button");

  const appendLoader = () => {
    const container = document.createElement("div");
    container.classList = "gumroad-loading-indicator";
    const icon = document.createElement("i");
    container.appendChild(icon);
    document.body.appendChild(container);
  };

  const appendGumroadContainer = () => {
    const container = document.createElement("div");
    container.classList = "gumroad-scroll-container";
    container.style.maxWidth = window.innerWidth + "px";
    container.style.maxHeight = window.innerHeight + "px";
    container.style.width = "0px";
    container.style.height = "0px";
    const productFrame = document.createElement("iframe");
    productFrame.setAttribute("scrolling", "no");
    productFrame.setAttribute("allowfullscreen", "allowfullscreen");
    productFrame.setAttribute("allowpaymentrequest", "allowpaymentrequest");
    productFrame.setAttribute(
      "src",
      "https://gumroad.com/overlay_page?all_permalinks=demo"
    );
    productFrame.setAttribute("class", "gumroad-overlay-iframe");
    productFrame.setAttribute("id", "productIframe");
    productFrame.setAttribute("allowTransparency", "true");
    productFrame.style.position = "absolute";
    productFrame.style.minWidth = "80%";
    productFrame.style.minHeight = "80%";
    container.appendChild(productFrame);
    document.body.appendChild(container);
  };

  const handleOnLoad = (iFrame) => {
    container = document.getElementsByClassName(
      "gumroad-scroll-container"
    )[0];
    container.style.width = window.innerWidth + "px";
    container.style.height = window.innerHeight + "px";
    container.style.display = "flex";
    container.style.justifyContent = "center";
    container.style.alignItems = "center";
    container.addEventListener("click", (e) => {
      container.style.display = "none";
      loader.style.display = "none";
      iFrame.src = "";
    });

    container.append(iFrame);
    container.style.backgroundColor = "rgba(0, 0, 0, 0.7)";
  }

  const addClickEvent = (button) => {
    button.addEventListener("click", function (e) {
      e.preventDefault();
      loader = document.querySelector(".gumroad-loading-indicator");
      loader.style.display = "block";
      // show iframe
      const iFrame = document.getElementById("productIframe");
      iFrame.src = this.href;
      iFrame.width = "70%";
      iFrame.height = "95%";
      iFrame.onload = handleOnLoad(iFrame);
    });
  };

  if (typeof buttons != "undefined") {
    appendLoader();
    appendGumroadContainer();
    buttons.forEach(addClickEvent);
  }
})();
